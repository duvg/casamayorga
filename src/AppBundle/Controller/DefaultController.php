<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\ContactType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath(
                $this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }

    /**
     * @Route("/quienessomos", name="quienessomos")
     */
    public  function quinessomosAction()
    {
        return $this->render('default/misionvision.html.twig');
    }

    /**
     * @Route("/servicios", name="servicios")
     */
    public function serviciosAction()
    {
        return $this->render('default/servicios.html.twig');
    }

    /**
     * @Route("/productos", name="productos")
     */
    public function productosAction()
    {
        return $this->render('default/productos.html.twig');
    }

    /**
     * @Route("/construccion", name="construccion")
     */
    public  function construccionAction()
    {
        return $this->render('default/construccion.html.twig');
    }

    /**
     * @Route("/contactar", name="contactar")
     */
    public function contactAction(Request $request)
    {
        $form = $this->createForm(new ContactType());

        if($request->isMethod('POST'))
        {
            $form->handleRequest($request);

            if($form->isValid())
            {
                /* send mail by smtp
                $mailer = $this->get('mailer');
                $message = $mailer->createMessage()
                    ->setSubject('Nuevo mensaje de '.$form->get('nombre')->getData())
                    ->setFrom("system@casamayorga.com")
                    ->setTo('contactenos@casamayorga.com')
                    ->setBody(
                        $this->renderView(
                            'default/mail/contactar.html.twig',
                            array(
                                'nombre' => $request->get('nombre'),
                                'apellidos' => $request->get('epallidos'),
                                'email' => $request->get('email'),
                                'telefono' => $request->get('telefono'),
                                'mensaje' => $request->get('mensaje')
                            )
                        )
                    );

                $mailer->send($message);
                */

                $para      = 'contactenos@casamayorga.com';
                $titulo    = 'Nuevo mensaje de contacto - '.$request->get('nombre');
                $mensaje   = $request->get('nombre')." ".
                    $request->get('epallidos')."\nTelefono: ".$request->get('telefono').
                    "\nEmail: ".$request->get('email')."\n \n".
                    $request->get('mensaje');


                $cabeceras = 'From: webmaster@casamayorga.com' . "\r\n" .
                    'Reply-To: ' .$request->get('email'). "\r\n" .
                    'X-Mailer: PHP/' . phpversion();

                mail($para, $titulo, $mensaje, $cabeceras);

                return $this->redirect($this->generateUrl('success_contact'));

            }
        }

        return $this->render('default/contactar.html.twig',  array('form' => $form->createView()));
    }

    /**
     * @Route("/enviado", name="success_contact")
     */
    public function successAction()
    {
        return $this->render('default/success.html.twig');
    }
}
