<?php
/**
 * Created by PhpStorm.
 * User: duviel
 * Date: 6/11/17
 * Time: 8:23 PM
 */

namespace AppBundle\Form\EventListener;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityRepository;

class AddDepartamentoFieldSubscriber implements EventSubscriberInterface
{
    private $factory;

    public function __construct(FormFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_BIND     => 'preBind'
        );
    }

    private function addCountryForm($form, $country)
    {
        $form->add($this->factory->createNamed('departamento', 'entity', $country, array(
            'class'         => 'AppBundle:Departamento',
            'auto_initialize' => false,
            'mapped'        => false,
            'empty_value'   => 'Departamento',
            'query_builder' => function (EntityRepository $repository) {
                $qb = $repository->createQueryBuilder('departamento');

                return $qb;
            }
        )));


    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $country = ($data->getMunicipio()) ? $data->getMunicipio()->getDepartamento() : 01 ;
        $this->addCountryForm($form, $country);
    }

    public function preBind(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $country = array_key_exists('departamento', $data) ? $data['departamento'] : null;
        $this->addCountryForm($form, $country);
    }
}