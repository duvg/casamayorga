<?php
/**
 * Created by PhpStorm.
 * User: duviel
 * Date: 6/11/17
 * Time: 8:27 PM
 */

namespace AppBundle\Form\EventListener;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityRepository;

use Doctrine\ORM\EntityManager;
class AddMunicipioFieldSubscriber implements EventSubscriberInterface
{
    private $factory;

    public function __construct(FormFactoryInterface $factory)
    {
        $this->factory = $factory;

    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_BIND     => 'preBind'
        );
    }

    private function addCityForm($form, $province)
    {
        $form->add($this->factory->createNamed('municipio','entity', null, array(
            'class'         => 'AppBundle:municipio',
            'auto_initialize' => false,
            'empty_value'   => 'Municipio',
            'query_builder' => function (EntityRepository $repository) use ($province) {
                $qb = $repository->createQueryBuilder('municipio')
                    ->innerJoin('municipio.departamento', 'departamento');
                if ($province instanceof Province) {
                    $qb->where('municipio.departamento = :departamento')
                        ->setParameter('departamento', $province);
                } elseif (is_numeric($province)) {
                    $qb->where('departamento.id = :departamento')
                        ->setParameter('departamento', $province);
                } else {
                    $qb->where('departamento.nombre = :departamento')
                        ->setParameter('departamento', null);
                }

                return $qb;
            }
        )));
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $province = ($data->municipio) ? $data->municipio->getProvince() : null ;
        $this->addCityForm($form, $province);
    }

    public function preBind(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $province = array_key_exists('departamento', $data) ? $data['departamento'] : null;
        $this->addCityForm($form, $province);
    }
}