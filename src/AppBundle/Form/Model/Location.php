<?php
/**
 * Created by PhpStorm.
 * User: duviel
 * Date: 6/11/17
 * Time: 8:59 PM
 */

namespace AppBundle\Form\Model;


use Symfony\Component\Validator\Constraints as Assert;

class Location
{
    /**
     * @Assert\NotBlank()
     */
    public $address;

    /**
     * @Assert\Type("AppBundle\Entity\Municipio")
     * @Assert\NotNull()
     */
    public $municipio;
}