<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PqrsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        $builder
            ->add('nombre')
            ->add('tipoDocuemnto','choice', array(
                'choices' => array(
                    ''   => 'Seleccione...',
                    'RI' => 'Registro Civil',
                    'TI' => 'Tarjeta de Identidad',
                    'CC' => 'CC'
                )
            ))
            ->add('nidenficacion')
            ->add('afiliado', 'choice', array(
                'choices' => array(
                    'Si' => 1,
                    'No' => 0
                ),
                'label' => 'Esta afiliado',
                'required' => true,
                'empty_value' => false,
                'choices_as_values' => true,
                'expanded' => true
            ))
            ->add('email')
            ->add('direccion')
            ->add('telefono')
            ->add('celular')
            /*
            ->add('tipodocumentoAfe', 'choice', array(
                'choices' => array(
                    'RI' => 'Registro Civil',
                    'TI' => 'Tarjeta de Identidad',
                    'CC' => 'CC'
                )
            ))
            ->add('ndocumentoAfe')
            ->add('nombreAfe')
            ->add('direccionAfe')
            ->add('telefonoAfe')*/
            ->add('lugarServicio')
            ->add('funcionario')
            ->add('asunto')
            ->add('mensaje', 'textarea',array(
                'attr' => array(
                    'cols' => 90,
                    'rows' => 7,
                    'placeholder' => 'Mensaje que deseas enviar a casamayorga'
                )
            ))
            ->add('fecha', 'date', array(
                'widget' => 'single_text'
            ))
            ->add('departamento','entity', array(
                'class' => 'AppBundle:Departamento',
                'empty_value' => 'Seleccione departamento'
            ))
            ->add('municipio', 'shtumi_dependent_filtered_entity', array(
                'entity_alias' => 'municipio_by_departamento',
                'empty_value'=> 'Seleccionar municipio',
                'parent_field'=>'departamento'
            ))
            ->add('tiposolicitud')
            ->add('enviar', 'submit');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Pqrs'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_pqrs';
    }


}
